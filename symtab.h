#ifndef  SYMTAB_H_
#define SYMTAB_H_
#include <string>
#include <fstream>
#include "Node.h"
#include "Relocation.h"

using namespace std;

struct Node;
struct Relocation;

struct Section {
	string name;
	int size;
	int num;

	Node* content;
	Node* content_tail;
	Relocation* rel;
	Relocation* rel_tail;
	Section* next;
	Section(string name,int num) : content(nullptr), content_tail(nullptr),rel(nullptr),rel_tail(nullptr), next(nullptr) {
		this->name = name;
		this->num = num;
	}
	~Section();
};

struct Forwardrefs {
	Node* node;
	Section* section;
	int offset;//0 or 1
	Forwardrefs* next = nullptr;
	Forwardrefs(Node* node, Section* section, int offset);
	~Forwardrefs() {
		node = nullptr; section = nullptr;
	}
};

struct Symbol {
	string name;
	Section* section;
	int val;
	bool global;
	Symbol* equ = nullptr;
	Symbol* next;
	int num;

	bool defined;
	Forwardrefs* flink;

	Symbol(string name, Section* s = nullptr, int v = -1, bool g = false,
		Symbol* next = nullptr,int num = 0, bool def = false, Forwardrefs* fl = nullptr);
	~Symbol();
};


void addSection(string name);
void toContent(Node* node);
void toSection(Section* s, Relocation* rel);

Symbol* search(string name);
void addSymbol(string name, int equ = 0);
Symbol* addReference(Node* node,int index, Symbol* sym, string& name);
void resolveRefs();

void writeOutput(ofstream& f,ofstream& output);
void deleteStructs();
#endif // ! SYMTAB_H_



