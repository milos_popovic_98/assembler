#ifndef RELOCATION_H_
#define RELOCATION_H_
#include "symtab.h"
#include "Node.h"

struct Node;
struct Symbol;
struct Section;

struct Relocation {
	int offset;
	Node* node;
	Symbol* symbol;
	int index;
	int type;//0-abs,1-pcrel
	Relocation* next = nullptr;
	Relocation(int offset, Node* node, Symbol* symbol, int type, int index);
	~Relocation() { symbol = nullptr; }
};
void addRelocation(Section* section, int offset, Node* node, Symbol* symbol, int type, int index);
void updateRelocations();
#endif
