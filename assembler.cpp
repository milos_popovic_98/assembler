#include "assembler.h"
#include <iostream>
#include <string>
#include <regex>
#include <map>
#include <iterator>
#include <vector>
#include "symtab.h"
#include "Equ.h"
#include "Relocation.h"


using namespace std;

vector<string> global;
vector<string> und;
static bool stop = false;

extern map<string, int>instructions;
extern Section* currentSection;
extern Section* UND;
extern int lctr;

void processDirective(string& str);
void processLabel(string str);
void processInstr(string str);


void process(ifstream& file)
{
    static regex directive("[.].+");
    static regex label("[^:]+:[^:]*");
    static regex instruction(".+");

    string str;
    
    while (!stop && getline(file, str)) {
        if (regex_match(str, directive)) processDirective(str);
        else if (regex_match(str, label)) processLabel(str);
        else if (regex_match(str,instruction)) processInstr(str);
    }
}
string& removeSpaces(string& str)
{
    str.erase(remove(str.begin(), str.end(), ' '), str.end());
    return str;
}
void processDirective(string& str) {
    regex num("^[0-9]+$");
    regex hnum("^0x[0-9,A-F,a-f]+$");
    str = str.substr(1);
    
    size_t i = str.find(" ");

    string dir = str.substr(0, i);
    if (i!=string::npos)
    str = str.substr(i);

    if (dir == "global") {
        str = removeSpaces(str);
        str = str.append(",");
        i = str.find(",");
        while (i != string::npos ) {
            if (i == 0) return;
            global.push_back(str.substr(0, i));
            str = str.substr(i+1);
            i = str.find(",");
        }
    }
    else if (dir == "extern") {
        str = removeSpaces(str);
        str = str.append(",");
        i = str.find(",");
        while (i != string::npos) {
            if (i == 0) return;
            und.push_back(str.substr(0, i));
            str = str.substr(i + 1);
            i = str.find(",");
        }
    }
    else if (dir == "equ") {
        str = removeSpaces(str);
        makeEqu(str);
    }
    else if (dir == "section") {
        str = removeSpaces(str);
        if (str != "") addSection(str);
        else {
            cout << "Sekcija nema ime";
            exit(-1);
        }
    }
    else if (dir == "end") {
        currentSection->size = lctr;
        stop = true;
    }
    else if (dir == "skip") {
        str = removeSpaces(str);
        if (regex_match(str, num) || regex_match(str,hnum)) {
            addData(str, 3);
        }
        else {
            cout << "Nakon skip direktive se navodi broj bajtova";
            exit(-1);
        }
    }
    else if (dir == "byte") {
        str = removeSpaces(str);
        str.append(",");
        i = str.find(",");
        while (i != string::npos) {
            if (i > 0) {
                addData(str.substr(0, i), 1);
           }
            str = str.substr(i + 1);
            i = str.find(",");
        }
    }
    else if ("dir == word") {
        str = removeSpaces(str);
        str.append(",");
        i = str.find(",");
        while (i != string::npos) {
            if (i > 0) {
                addData(str.substr(0, i), 2);
            }
            str = str.substr(i + 1);
            i = str.find(",");
        }
    }
}

void processLabel(string str) {
    static regex r("^ *$");
    static regex dir("^ *[.].+");

    size_t i = str.find(":");
    string symbol = str.substr(0, i);
    addSymbol(symbol);
    str = str.substr(i + 1);
    if (!regex_match(str,r)) {
        str = str.substr(1);
        if (regex_match(str, dir)) {
            processDirective(str);
        }
        processInstr(str);
    }
}

void processInstr(string str) {
    static regex  param1("[^,]+");
    static regex param2("[^,]+,[^,]+");
    static regex param0(" *");

    bool b=false;

    if (!currentSection) {
        cout << "Instrukcija nije unutar sekcije";
        exit(-1);
    }
    size_t i = str.find(" ");
    string name = str.substr(0, i);
    if (i != string::npos) {
        if (str[i - 1] == 'w') name = name.substr(0, i - 1);
        else if (str[i-1] == 'b' && name != "sub") {
            name = name.substr(0, i - 1);
            b = true;
        }
    }
    map<string,int>::iterator iter = instructions.find(name);
    if (iter == instructions.end()) return;

    if (i != string::npos) str = str.substr(i+1);
    if (iter->second < 3 && (regex_match(str, param0) || i==string::npos)) {
        addNode(iter->second, 0, str, b);
    }
    else if (iter->second > 10 && regex_match(str, param2)) {
        addNode(iter->second, 2, str, b);
    }
    else if (regex_match(str, param1)) {
        addNode(iter->second, 1, str, b);
    }
    else {
        cout << "invalid syntax " + str << endl;
        exit(-1);
    }
}
void addGlobal() {
    size_t size = global.size();
    size_t i;
    
    for (i = 0; i < size; i++) {
        Symbol* s = search(global[i]);
        if (!s){
            cout << "Globalni simbol nije definisan";
            exit(-1);
        }
        s->global = true;
    }
}
void addExtern() {
    size_t size = und.size();
    size_t i = 0;

    for (i = 0; i < size; i++) {
        Symbol* s = search(und[i]);
        if (s && s->defined){
            cout << "Uvezeni simbol je definisan unutar fajla";
            exit(-1);
        }
        if (!s) {
            addSymbol(und[i]);
            s = search(und[i]);
        }
        s->defined = true;
        s->global = true;
        s->section = UND;
        s->val = 0;
    }
}
void backpatching() {
    addGlobal();
    addExtern();
    processEqu();
    resolveRefs();
    updateRelocations();
}




