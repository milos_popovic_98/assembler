#ifndef INSTRUCTIONS_H_
#define INSTRUCTIONS_H_

#include <map>
#include <string>

using namespace std;


void init(map<string, int>& instructions);
bool decToHex(int n, char* hex, int byte = 0);
char* writeHex(int n, int num);

#endif

