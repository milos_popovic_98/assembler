#ifndef EQU_H_
#define EQU_H_
#include "symtab.h"
#include <vector>

struct Symbol;

struct Elem {
	string name;
	int sign;//-=>-1,+=>1
	Elem(string n, int sign):name(n) {
		this->sign = sign;
	}
};
struct Equ {
	string name;
	vector<Elem> records;
	Equ* next = nullptr;
	int absolut;
	Equ(string name) {
		this->name = name;
		absolut = 0;
	}
};
void makeEqu(string str);
void processEqu();



#endif
