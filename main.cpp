#include "instructions.h"
#include "assembler.h"
#include "symtab.h"
#include <iostream>
#include <fstream>


using namespace std;

map<string, int> instructions;

int main(int argc, char* argv[]) {
    if (argc < 3) return -2;
	init(instructions);
    cout << argv[1] << " " << argv[2];
    ifstream file(argv[1]);
    ofstream output(argv[2]);
    ofstream outfile;
    outfile.open("binarni.dat", ios::binary | ios::out);
    process(file);
    backpatching();
    writeOutput(outfile,output);

    deleteStructs();
    file.close();
    outfile.close();

    return 0;

}