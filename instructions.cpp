#include "instructions.h"
#include <iostream>

using namespace std;

static int NUM_OF_INSTRUCTIONS = 25;

static string* names = new string[NUM_OF_INSTRUCTIONS]{
	"halt","iret","ret","int","call","jmp","jeq","jne","jgt","push","pop","xchg","mov",
	"add","sub","mul","div","cmp","not","and","or","xor","test","shl","shr"
};

void init(map<string, int>& instructions)
{
	for (int i = 0; i < NUM_OF_INSTRUCTIONS; i++) {
		instructions.insert(pair<string, int>(names[i], i));
	}
}
inline void swap(char& c1, char& c2) {
    char t = c1;
    c1 = c2; c2 = t;
}
bool decToHex(int n, char* hex, int num)
{
    if (n < 0) {
        n = 0xFFFF + n + 1;
    }
    for (int i = 0, tmp = 0; i < num; i++) {
            tmp = n % 16;
            if (tmp < 10) {
                hex[i] = tmp + 48;
            }
            else {
                hex[i] = tmp + 55;
            }
            if (i % 2) swap(hex[i - 1], hex[i]);
            n = n / 16;
   }
   return n > 0;
}
char* writeHex(int n, int num) {
    char* hex = new char[num];
    decToHex(n, hex, num);
    return hex;
}









