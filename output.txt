
#tabela simbola
#ime  sek  vr  l/g rbr size
ivt_table   1   0   l    1    8 
init   2   0   l    2    8 
fault   3   0   l    3    8 
timer_interrupt   4   0   l    4    8 
terminal_interrupt   5   0   l    5    6 
init     2   0   l    6
fault     3   0   l    7
timer_interrupt     4   0   l    8
terminal_interrupt     5   0   l    9

#Rel.Section ivt_table
#offset   tip   vrednost
0     R_386_32       2
2     R_386_32       3
4     R_386_32       4
6     R_386_32       5

#Section ivt_table
00 00 00 00 00 00 00 00 
#Section init
64 00 05 00 80 10 FF 08 
#Section fault
64 00 FF FF 80 00 FF 00 
#Section timer_interrupt
64 00 00 00 80 00 FF 08 
#Section terminal_interrupt
64 80 02 FF 22 08 