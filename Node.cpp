#include "Node.h"
#include "symtab.h"
#include "Relocation.h"
#include "assembler.h"
#include "instructions.h"
#include <regex>
#include <iostream>

using namespace std;

extern int lctr;
extern Section* currentSection;
void processParam(string str, Node_instr* node, int index, int dst, bool b, int jmp);

void addNode(int instr,int numOfOperands,string& str,bool b)
{
    Node_instr* node = new Node_instr(currentSection,lctr);
    node->instrDescr = instr << 3;
    if (!b && numOfOperands>0) {
        node->instrDescr |= 1 << 2;
    }
    lctr++;
    if (numOfOperands == 1) {
        node->params[0] = new Param();
        
        str = removeSpaces(str);
        if (instr < 9) processParam(str, node,0,0, false, 1);
        else if (instr == 9) processParam(str, node, 0, 0, b, 0);
        else processParam(str, node, 0, 1, b, 0);
    }
    else if (numOfOperands == 2) {
        node->params[0] = new Param();
        node->params[1] = new Param();

        size_t i = str.find(",");
        string op1 = str.substr(0, i);
        string op2 = str.substr(i + 1);
        op1 = removeSpaces(op1);
        op2 = removeSpaces(op2);
        if (instr == 24) {
            processParam(op1, node, 0, 1, b, 0);
            processParam(op2, node, 1, 0, b, 0);
        }
        else {
            processParam(op1, node, 0, 0, b, 0);
            processParam(op2, node, 1, 1, b, 0);
        }
    }

    toContent(node);

}
inline int sym_val(string name,Node* node,int index,int type) {
    Symbol* s = search(name);
    int ret;
    if (s && s->defined) {
        ret = s->val;
    }
    else {
        ret = 0;
        s= addReference(node, index, s, name);
    }
    addRelocation(currentSection, lctr, node, s, type, index);
    return ret;
}
inline int reg_descr(char* c, int type) {
    int reg = atoi(c);
    int descr = 0;

    descr = (type << 5) | (reg << 1);
    return descr;
}
inline int strToInt(string str) {
    int ret;
    if (str.length() > 2 && str[0] == '0' && str[1] == 'x') {
        str = str.substr(2);
        ret = stoi(str, nullptr, 16);
    }
    else ret = stoi(str);
    return ret;
}
void processParam(string str, Node_instr* node, int index, int dst, bool b, int jmp) {
    static regex literal_imm("^[$][-]*[0-9]+$");
    static regex hliteral_imm("^[$]0x[0-9,A-F,a-f]+$");
    static regex jliteral_imm("^[-]*[0-9]+$");
    static regex hjliteral_imm("^0x[0-9,A-F,a-f]+$");

    static regex symbol_imm("^[$][a-z,A-Z,_][0-9,a-z,A-Z,_]*$");
    static regex jsymbol_imm("^[a-z,A-Z,_][0-9,a-z,A-Z,_]*$");
    
    static regex reg_direct("^%r[0-8][l,h]*$");
    static regex jreg_direct("^[*]%r[0-8]$");
    
    static regex reg_indirect("^[(]%r[0-8][)]$");
    static regex jreg_indirect("^[*][(]%r[0-8][)]$");
    
    static regex literal_offset("^[-]*[0-9]+[(]%r[0-8][)]$");
    static regex hliteral_offset("^0x[0-9,A-F,a-f]+[(]%r[0-8][)]$");
    static regex jliteral_offset("^[*][-]*[0-9]+[(]%r[0-8][)]$");
    static regex jhliteral_offset("^[*]0x[0-9,A-F,a-f]+[(]%r[0-8][)]$");
    
    static regex symbol_offset("^[a-z,A-Z,_][0-9,a-z,A-Z,_]*[(]%r[0-6,8][)]$");
    static regex jsymbol_offset("^[*][a-z,A-Z,_][0-9,a-z,A-Z,_]*[(]%r[0-6,8][)]$");
    
    static regex pc_rel1("^[a-z,A-Z,_][0-9,a-z,A-Z,_]*[(]%pc[)]$");
    static regex jpc_rel1("^[*][a-z,A-Z,_][0-9,a-z,A-Z,_]*[(]%pc[)]$");
    static regex pc_rel2("^[a-z,A-Z,_][0-9,a-z,A-Z,_]*[(]%r7[)]$");
    static regex jpc_rel2("^[*][a-z,A-Z,_][0-9,a-z,A-Z,_]*[(]%r7[)]$");

    static regex literal_mem("^[-]*[0-9]+$");
    static regex hliteral_mem("^0x[0-9,A-F,a-f]+$");
    static regex jliteral_mem("^[*][-]*[0-9]+$");
    static regex jhliteral_mem("^[*]0x[0-9,A-F,a-f]+$");
    
    static regex symbol_mem("^[a-z,A-Z,_][a-z,A-Z,0-9,_]*$");
    static regex jsymbol_mem("^[*][a-z,A-Z,_][a-z,A-Z,0-9,_]*$");
    
   
    if ((!jmp && (regex_match(str, literal_imm) || regex_match(str,hliteral_imm))) || 
        (jmp && (regex_match(str,jliteral_imm) || regex_match(str,hjliteral_imm)))) {
        if (dst == 1) {
            cout << "Neposredno adresiranje odredisnog operanda nije dozvoljeno "<<str<<" ";
            exit(-1);
        }
        str = str.substr(1-jmp);
        int v=strToInt(str);
       
        if (b) {
            node->params[index]->size = 2;
        }
        node->params[index]->value = v;
        node->params[index]->opDescr = 0;
        lctr += node->params[index]->size;
    }
    else if ((!jmp && regex_match(str, symbol_imm)) || (jmp && regex_match(str,jsymbol_imm))) {
        if (dst == 1) {
            cout << "Neposredno adresiranje odredisnog operanda nije dozvoljeno "<<str<<" ";
            exit(-1);
        }
        str = str.substr(1-jmp);
        node->params[index]->opDescr = 0;
        if (b) {
            node->params[index]->size = 2;
        }
        lctr += 1;
        node->params[index]->value = sym_val(str, node, index,0);

        lctr += 2;
    }
    else if ((!jmp && regex_match(str, reg_direct)) || (jmp && regex_match(str,jreg_direct))) {
        char c = str[2+jmp];
        
        node->params[index]->opDescr = reg_descr(&c,1);
        node->params[index]->size = 1;
        if (b && str[3] == 'h') {
            node->params[index]->opDescr |= 1;
        }
        lctr += 1;
    }
    else if ((!jmp && regex_match(str, reg_indirect)) || (jmp && regex_match(str,jreg_indirect))) {
        char c = str[3+jmp];
        
        node->params[index]->opDescr = reg_descr(&c,2);
        node->params[index]->size = 1;
        
        lctr += 1;
    }
    else if ((!jmp && (regex_match(str, literal_offset) || regex_match(str,hliteral_offset))) 
        || (jmp && (regex_match(str,jliteral_offset) || regex_match(str,jhliteral_offset)))) {
        int i = str.find("(");
        string lit = str.substr(0+jmp, i-jmp);
        node->params[index]->value = strToInt(lit);

        char c = str[i + 3];
        node->params[index]->opDescr = reg_descr(&c, 3);
        
        lctr += 3;
    }
    else if ((!jmp && regex_match(str, symbol_offset)) || (jmp && regex_match(str,jsymbol_offset))) {
        int i = str.find("(");
        string name = str.substr(0+jmp, i-jmp);
        lctr += 1;
        node->params[index]->value = sym_val(name, node, index,0);

        char c = str[i + 3];
        node->params[index]->opDescr = reg_descr(&c, 3);

        lctr += 2;
    }
    else if ((!jmp && regex_match(str, pc_rel1)) || (jmp && regex_match(str,jpc_rel1)) 
        || (!jmp && regex_match(str, pc_rel2)) || (jmp && regex_match(str,jpc_rel2))) {
        int i = str.find("(");
        string name = str.substr(0+jmp, i-jmp);
        lctr += 1;
        sym_val(name, node, index, 1);
        
        if (index == 0) {
            node->params[index]->value = -5;
        }
        else node->params[index]->value = -2;

        char c = '7';
        node->params[index]->opDescr = reg_descr(&c, 3);

        lctr += 2;
    }
    else if ((!jmp && (regex_match(str, literal_mem)|| regex_match(str,hliteral_mem))) || 
        (jmp && (regex_match(str,jliteral_mem) || regex_match(str,jhliteral_mem)))) {
        str = str.substr(jmp);
        int v = strToInt(str);
        node->params[index]->value = v;
        node->params[index]->opDescr = 4 << 5;
        
        lctr += 3;
    }
    else if ((!jmp && regex_match(str, symbol_mem)) || (jmp && regex_match(str,jsymbol_mem))) {
        node->params[index]->opDescr = 4 << 5;
        lctr += 1;
        node->params[index]->value = sym_val(str.substr(jmp), node, index, 0);

        lctr += 2;
    }
    else {
     cout << "Nepoznat nacin adresiranja "<<str<<endl;
     exit(-1); 
    }
}
void addData(string data, int type) {
    regex num("^[-]*[0-9]+$");
    regex hnum("^0x[0-9,A-F,a-f]+$");
    regex sym("^[a-z,A-Z,_][a-z,A-Z,0-9,_]*$");
    Node_data* node = new Node_data(type,currentSection,lctr);
    if (type == 3) {
        node->data = strToInt(data);
        lctr += node->data;
    }
    else {
        if (regex_match(data, num) || regex_match(data,hnum)) {
            node->data = strToInt(data);
        }
        else if (regex_match(data, sym)) {
            node->data = sym_val(data, node, 0, 0);
        }
        if (type == 2) lctr += 2;
        else lctr++;
    }

    toContent(node);
}

void Node_data::setVal(int val, int index)
{
    this->data = val;
}

void Node_data::write(ofstream& f,ofstream& fout)
{
    char* c;
    if (type == 1) {
        c = writeHex(data, 2);
        fout << c[0] << c[1] << " ";
        int8_t d = data;
        f.write((const char*)&d, sizeof(int8_t));
    }
    else if (type == 2) {
        c = writeHex(data, 4);
        fout << c[0] << c[1] << " " << c[2] << c[3] << " ";
        int16_t d = data;
        f.write((const char*)&d, sizeof(int16_t));
    }
    if (type == 3) {
        int8_t d = 0;
        for (int i = 0; i < data; i++) {
            fout << "00 ";
            f.write((const char*)&d, sizeof(int8_t));
        }
    }
}

void Node_instr::setVal(int val, int index)
{
    this->params[index]->value = val;
}

void Node_instr::write(ofstream& f,ofstream& fout)
{
    int8_t b;
    int16_t w;
    char* c;
    c = writeHex(instrDescr, 2);
    b = instrDescr;
    f.write((const char*)&b, sizeof(int8_t));
    fout << c[0] << c[1] << " ";
    if (params[0]) {
        c = writeHex(params[0]->opDescr, 2);
        fout << c[0] << c[1] << " ";
        b = params[0]->opDescr;
        f.write((const char*)&b, sizeof(int8_t));
        if (params[0]->size == 2) {
            c = writeHex(params[0]->value, 2);
            fout << c[0] << c[1] << " " ;
            b = params[0]->value;
            f.write((const char*)&b, sizeof(int8_t));
        }
        else if (params[0]->size == 3) {
            c = writeHex(params[0]->value, 4);
            fout << c[0] << c[1] << " " << c[2] << c[3] << " ";
            w = params[0]->value;
            f.write((const char*)&w, sizeof(int16_t));
        }
        if (params[1]) {
            c = writeHex(params[1]->opDescr, 2);
            fout << c[0] << c[1] << " ";
            b = params[1]->opDescr;
            f.write((const char*)&b, sizeof(int8_t));

            if (params[1]->size == 2) {
                c = writeHex(params[1]->value, 2);
                fout << c[0] << c[1] << " ";
                b = params[1]->value;
                f.write((const char*)&b, sizeof(int8_t));
            }
            if (params[1]->size == 3) {
                c = writeHex(params[1]->value, 4);
                fout << c[0] << c[1] << " " << c[2] << c[3] << " ";
                w = params[1]->value;
                f.write((const char*)&w, sizeof(int16_t));
            }
        }
    }
}









