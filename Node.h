#ifndef NODE_H_
#define NODE_H_
#include <string>
#include "symtab.h"
#include <vector>
using namespace std;

struct Section;

struct Param {
	int opDescr;
	int value;
	int size = 3; //1,2 or 3
};
struct Node {
	int offset;
	Section* section;
	Node* next;
	virtual void setVal(int val, int index)=0;
	virtual void write(ofstream& f,ofstream& fout) = 0;
	Node(Section* s, int offs) :section(s), next(nullptr) { this->offset = offs; }
	~Node() { section = nullptr; next = nullptr; }
};
struct Node_instr : Node{
	int instrDescr;
	Param** params;
	Node_instr(Section* s,int offs): Node(s,offs) {
		params = new Param* [2];
		params[0] = params[1] = nullptr;
	}
	void setVal(int val, int index) override;
	void write(ofstream& f,ofstream& fout) override;
	~Node_instr() { params[0] = params[1] = nullptr; params = 0; }
};
struct Node_data :Node {
	int data;
	int type;//byte=1,word=2,skip=3
	Node_data(int t,Section* s,int offs) :Node(s,offs){
		type = t;
	}
	void setVal(int val, int index) override;
	void write(ofstream& f,ofstream& fout) override;
};



void addNode(int instr, int numOfOperands, string& str,bool b);
void addData(string d, int type);
#endif
