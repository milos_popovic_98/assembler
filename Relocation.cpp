#include "Relocation.h"
#include "symtab.h"

extern Section* firstSection;
Relocation::Relocation(int offset, Node* node, Symbol* symbol, int type, int index):node(node),symbol(symbol)
{
	this->offset = offset;
	this->type = type;
	this->index = index;
}

void addRelocation(Section* section, int offset, Node* node, Symbol* symbol, int type, int index) {
	Relocation* r = new Relocation(offset, node, symbol, type, index);
	toSection(section, r);
}
void deleteRel(Section* s,Relocation*& rel, Relocation*& prev) {
	Relocation* old = rel;
	if (rel == s->rel) {
		s->rel = s->rel->next;
	}
	else {
		prev->next = rel->next;
	}
	rel = rel->next;
	delete old;
}
void updateRelocations()
{
	Relocation* rel, * prev = nullptr, * old = nullptr;
	Section* s = firstSection;
	
	while (s) {
		rel = s->rel;
		while (rel) {
			Symbol* sym = rel->symbol;
			if (rel->type == 0) {
				if (sym->section->name == "absolut") {
					rel->node->setVal(sym->val, rel->index);
					deleteRel(s, rel, prev);
				}
				else if (sym->section->name == "equ") {
					Symbol* equ = rel->symbol->equ;
					if (!(equ->global)) {
						rel->node->setVal(equ->val + sym->val, rel->index);
						rel->index = equ->section->num;
					}
					else {
						rel->node->setVal(sym->val, rel->index);
						rel->index = equ->num;
					}
					prev = rel;
					rel = rel->next;
				}
				else {
					if (sym->global) {
						rel->node->setVal(0, rel->index);
						rel->index = sym->num;
					}
					else rel->index = sym->section->num;

					prev = rel; rel = rel->next;
				}
			}
			else {
				if (sym->section->name == "absolut" || sym->section == rel->node->section) {
					int v = sym->val;
					if (!rel->node->next) v -= rel->node->section->size;
					else v -= rel->node->next->offset;
					rel->node->setVal(v,rel->index);
					deleteRel(s, rel, prev);
				}
				else if (sym->section->name == "equ") {
					int v = sym->val, ind = rel->index;
					if (!rel->node->next) v -= rel->node->section->size-rel->offset;
					else v -= rel->node->next->offset-rel->offset;
					Symbol* equ = sym->equ;
					if (equ->global) {
						rel->index = equ->num;
					}
					else {
						v += equ->val;
						rel->index = equ->section->num;
					}
					rel->node->setVal(v, ind);

					prev = rel; rel = rel->next;
				}
				else {
					int v = 0, ind = rel->index;
					if (!rel->node->next) v -= rel->node->section->size - rel->offset;
					else v -= rel->node->next->offset - rel->offset;
					if (sym->global) {
						rel->index = sym->num;
					}
					else {
						v += sym->val;
						rel->index = sym->section->num;
					}
					rel->node->setVal(v, ind);

					prev = rel; rel = rel->next;
				}
			}
		}
		s = s->next;
	}
}










