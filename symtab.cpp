#include "symtab.h"
#include "instructions.h"
#include <iostream>
#include <string>

Section* UND = new Section("UND", 0);
Section* firstSection = nullptr;
Section* currentSection = nullptr;
int lctr = 0;
int lastSym = 0;
int numOfSections = 0;
extern vector<string> global;
extern vector<string> und;
Symbol* head = nullptr;
Symbol* tail = nullptr;


void addSection(string name)
{
	numOfSections++;
	Section* s = new Section(name,numOfSections);
	
	if (currentSection) {
		currentSection->size = lctr;
		currentSection->next = s;

		currentSection = s;
		lctr = 0;
	}
	else {
		firstSection = currentSection = s;
	}
	
}

void toContent(Node* node)
{
	if (!currentSection->content) {
		currentSection->content = node;
	}
	else currentSection->content_tail->next = node;

	currentSection->content_tail = node;
}

void toSection(Section* s, Relocation* rel)
{
	if (!s->rel) {
		s->rel = rel;
	}
	else s->rel_tail->next = rel;

	s->rel_tail = rel;
}

Symbol* search(string name)
{
	Symbol* t = head;
	while (t != nullptr) {
		if (t->name == name) {
			break;
		}
		t = t->next;
	}
	return t;
}
void toSymTable(Symbol* s) {
	if (!head) {
		head = s;
	}
	else {
		tail->next = s;
	}
	tail = s;
}
Symbol* addReference(Node* node,int index,Symbol* sym,string& name) {
	Symbol* s = sym;
	if (!s) {
		s = new Symbol(name);
		toSymTable(s);
	}
	Forwardrefs* f = new Forwardrefs(node, currentSection, index);
	f->next = s->flink;
	s->flink = f;
	return s;
}
void addSymbol(string name,int equ)
{
	Symbol* s = search(name);
	if (s) {
		if (s->defined) {
			cout << "Visetruko definisan simbol " << name;
			exit(-1);
		}
		else {
			if (!equ) s->section = currentSection;
			s->val = lctr;
			s->defined = true;
		}
	}
	else {
		if (!equ) s = new Symbol(name, currentSection, lctr, false, nullptr,lastSym, true);
		else {
			s = new Symbol(name,nullptr,0,false,nullptr,lastSym,true);
		}
		toSymTable(s);
	}
}


Forwardrefs::Forwardrefs(Node* node, Section* section, int offset)
{
	this->node = node;
	this->section = section;
	this->offset = offset;
}

Symbol::Symbol(string name, Section* s, int v, bool g, Symbol* next, int num, bool def, Forwardrefs* fl)
{
	this->name = name;
	this->section = s;
	this->val = v;
	this->global = g;
	this->next = next;
	this->num = num;
	this->defined = def;
	this->flink = fl;
}
Symbol::~Symbol()
{
	Forwardrefs* curr = flink;
	while (curr != nullptr) {
		Forwardrefs* old = curr;
		curr = curr->next;
		delete old;
	}
}
inline void write(char* c,ofstream& fout) {
	bool zero = true;
	if (c[0] == '0' && c[1]=='0' && c[2]=='0' && c[3]=='0') fout << '0';
	else {
		if (c[2] != '0') {
			zero = false;
			fout << c[2] << c[3];
		}
		else if (c[3] != '0') {
			zero = false;
			fout << c[3];
		}
		if (!zero || c[0] != '0') {
			fout << c[0] << c[1];
		}
		else fout << c[1];
	}
	fout << " ";
}
void writeSection(Section* s,ofstream& f,ofstream& fout) {
	Node* node = s->content;
	fout << "\n#Section " << s->name << endl;
	while (node) {
		node->write(f,fout);
		node = node->next;
	}
}
void writeRelocations(Section* s,ofstream& f,ofstream& fout) {
	static string relabs = "R_386_32";
	static string relpc = "R_386_PC32";
	Relocation* rel = s->rel;
	int numOfRel = 0;
	if (rel) {
		fout << "\n#Rel.Section " << s->name << endl;
		fout << "#offset   tip   vrednost\n";
		while (rel) {
			numOfRel++;
			char* offs = writeHex(rel->offset, 4);
			write(offs,fout);
			if (rel->type == 0) {
				fout << "    " << relabs << "   ";
			}
			else fout << relpc << "   ";
			fout << "    " << rel->index << endl;
			rel = rel->next;
		}
	}
	f.write((const char*)&numOfRel, sizeof(int));
	rel = s->rel;
	while (rel) {
		f.write((const char*)&(rel->offset), sizeof(int));
		f.write((const char*)&(rel->type), sizeof(int));
		f.write((const char*)&(rel->index), sizeof(int));
		rel = rel->next;
	}
}

void writeSymTab(ofstream& f,ofstream& fout) {
	int len;
	Section* s = firstSection;
	fout << "\n#tabela simbola\n";
	fout << "#ime  sek  vr  l/g rbr size\n";
	f.write((const char*)&numOfSections, sizeof(int));
	while (s) {
		char* size = writeHex(s->size, 4);
		fout << s->name << "   " << s->num << "   0   " << "l" << "    ";
		fout << s->num<<"    ";
		write(size,fout);
		fout << endl;
		
		len = (s->name).size();
		f.write((const char*)&len, sizeof(int));
		f.write((s->name).c_str(), len);
		f.write((const char*)&(s->num),sizeof(int));
		f.write((const char*)&(s->size),sizeof(int));
		s = s->next;

	}
	Symbol* sym = head, * prev = nullptr;
	int numOfSymbols = global.size()+und.size();
	int currentSymbol = numOfSections + 1;
	f.write((const char*)&numOfSymbols, sizeof(int));
	while (sym) {
		if (sym->section->name != "absolut" && sym->section->name != "equ") {
			fout << sym->name << "     ";
			fout << sym->section->num << "   ";
			char* val = writeHex(sym->val, 4);
			write(val,fout);
			if (sym->global) fout << "  g    ";
			else fout << "  l    ";

			fout << sym->num<<endl;

			len = (sym->name).size();
			if (sym->global) {
				f.write((const char*)&len, sizeof(int));
				f.write((sym->name).c_str(), len);
				f.write((const char*)&(sym->section->num), sizeof(int));
				f.write((const char*)&currentSymbol, sizeof(int));
				f.write((const char*)&(sym->val), sizeof(int));
			}
			currentSymbol++;
		}
		sym = sym->next;
	}
}
void writeOutput(ofstream& f, ofstream& fout) {

	Section* s = firstSection;
	writeSymTab(f,fout);
	while (s) {
		writeRelocations(s,f,fout);
		s = s->next;
	}
	s = firstSection;
	while (s) {
		writeSection(s,f,fout);
		s = s->next;
	}
}

void deleteStructs()
{
	Symbol* scurr = head;
	while (scurr != nullptr) {
		Symbol* old = scurr;
		scurr = scurr->next;
		delete old;
	}
	Section* curr = firstSection;
	while (curr != nullptr) {
		Section* old = curr;
		curr = curr->next;
		delete old;
	}

}

void resolveRefs() {
	int num = numOfSections+1;
	Symbol* sym = head;
	Symbol* prev = nullptr;
	while (sym) {
		if (!sym->section) {
			cout << "Simbol (koji nije uvezen) nije definisan" << endl;
			exit(-1);
		}
		Forwardrefs* f = sym->flink;
		while (f) {
			if (!sym->global) {
				f->node->setVal(sym->val, f->offset);
			}
			else f->node->setVal(0, f->offset);
			f = f->next;
		}
		if (sym->section->name != "absolut" && sym->section->name!="equ") {
			sym->num = num++;
		}
		sym = sym->next;
	}
}

Section::~Section()
{
	Node* curr = content;
	while (curr != nullptr) {
		Node* old = curr;
		curr = curr->next;
		delete old;
	}
	content = content_tail = nullptr;
	Relocation* rcurr = rel;
	while (rcurr != nullptr) {
		Relocation* old = rcurr;
		rcurr = rcurr->next;
		delete old;
	}
}
