#include "Equ.h"
#include <map>
#include <regex>
#include <iterator>
#include <vector>
#include <iostream>


using namespace std;
Equ* equ_head = nullptr;
Equ* equ_tail = nullptr;

void makeEqu(string str)
{
	regex num("^[0-9]+$");
	int sign = 0;
	size_t i = str.find(",");
	string name = str.substr(0, i);
	addSymbol(name,1);
	Equ* equ = new Equ(name);
	str = str.substr(i + 1);
	str = str.append("+");
	i = str.find_first_of("+-");
	if (i == 0 ) {
		sign = (str[i] == '+') ? 1 : -1;
		str = str.substr(1);
		i = str.find_first_of("+-");
	}
	else sign = 1;
	while (i != string::npos) {
		name = str.substr(0, i);
		if (regex_match(name, num)) {
			if (sign == 1) equ->absolut += stoi(name);
			else equ->absolut -= stoi(name);

		}
		else {
			Elem e(name, sign);
			equ->records.push_back(e);
		}
		sign = (str[i] == '+') ? 1 : -1;
		str = str.substr(i + 1);
		i = str.find_first_of("+-");
	}
	if (!equ_head) equ_head = equ;
	else equ_tail->next = equ;
	equ_tail = equ;
	
}
bool processInstance(Equ* t) {
	struct Record {
		int val;
		int num;
		vector<Elem> el;
		Record(int val, int num, Elem e) {
			this->val = val; 
			this->num = num;
			el.push_back(e);
		}
	};
	size_t size = t->records.size(), i;
	map<string, Record> temp;
	map<string, Record>::iterator iter;
	vector<Elem> recordsNew;
	Symbol* s;
	bool resolved = true;
	for (i = 0; i < size; i++) {
		Elem e = t->records[i];
		s = search(e.name);
		if (!(s->section)) {//equ unresolved symbol yet
			resolved = false;
			recordsNew.push_back(e);
		}
		else if (s->section->name == "absolut") {
				t->absolut += s->val;
			}
		else {
			int v = e.sign * s->val;
			int num = e.sign;
			if (s->section->name == "UND") num = 1;
			string name = s->section->name;
			if (name == "equ") {
				t->absolut += v;
				v = e.sign * s->equ->val;
				name = s->equ->section->name;
			}
			iter = temp.find(name);
			if (iter != temp.end()) {
				iter->second.val += v;
				iter->second.num += num;
				iter->second.el.push_back(e);
			}
			else temp.insert(pair<string, Record>(name, Record(v, num, e)));
		}
	}
	if (resolved) {
		Symbol* equ = search(t->name);
		bool rel = false;
		for (iter = temp.begin(); iter != temp.end(); iter++) {
			if (iter->second.num == 0) t->absolut += iter->second.val;
			else if (iter->second.num == 1 && rel == false) {
				Elem& e = iter->second.el.front();
				s = search(e.name);
				equ->equ = s;
				rel = true;
			}
			else {
				cout << "Neispravan equ izraz";
				exit(-1);
			}
		}
		equ->defined = true;
		equ->val = t->absolut;
		if (!rel) {
			equ->section = new Section("absolut", -1);
		}
		else equ->section = new Section("equ", -2);
	}
	else {
		for (iter = temp.begin(); iter != temp.end(); iter++) {
			if (iter->second.num == 0) t->absolut += iter->second.val;
			else {
				vector<Elem> el = iter->second.el;
				recordsNew.insert(recordsNew.end(), el.begin(), el.end());
			}
		}
		t->records = recordsNew;
	}
	return resolved;
}
void processEqu() {
	
	Equ* t = equ_head, * prev = nullptr, * old = nullptr;
	bool done = true;
	bool modified = false;
	while (t) {
		old = nullptr;
		bool ret = processInstance(t);
		if (ret) {
			modified = true;
			if (!prev) {
				equ_head = equ_head->next;
			}
			else prev->next = t->next;
			old = t;
		}
		else {
			done = false;
			prev = t;
		}
		t = t->next;
		delete old;
		if (!t) {
			if (!done) {
				if (!modified) {
					cout << "equ direktive nisu u korektnom formatu";
					exit(-1);
				}
				else {
					prev = nullptr; t = equ_head;
				}
				done = true; modified = false;
			}
			else break;
		}
	}
}














